import React, { forwardRef, useEffect, useImperativeHandle, useState } from 'react'
import { DetailComponent } from '../detail'
import { CardContainer, CardHeader, CardMainImg, ElectricType, FireType, ImagenContainer, InfoContainer, NumberInfo, OtherType, PlantType, PreFormLabel, TypesContainer, WaterType } from '../styles/card'
import { CardsContainer, ContentContainer } from '../styles/content'
import fondoPkm from '../assets/fondo.jpg'

export const ContentComponent = forwardRef(({ list }, ref) => {
    const [detail, setDetail] = useState(false)
    const [selectedPokemon, setSelectedPokemon] = useState()

    const openDetail = pkm => {
        setDetail(!detail)
        setSelectedPokemon(pkm)
    }

    useImperativeHandle(ref, () => ({
        openDetail(pkm) {
            setDetail(!detail)
            setSelectedPokemon(pkm)
        }
    }))

    useEffect(() => {
        console.log('selectedPokemon: ', selectedPokemon);
    }, [selectedPokemon])

    if (detail) {
        return (
            <ContentContainer>
                {selectedPokemon ?
                    <DetailComponent pokemon={selectedPokemon} closeDetail={openDetail} />
                    : null}
            </ContentContainer>)
    } else {
        return (<ContentContainer>
            <CardsContainer>
                {list.map(val => {
                    const pokemon = val.data
                    return (
                        <CardContainer key={pokemon.name} onClick={e => openDetail(pokemon)}>
                            <CardHeader>{pokemon.name.toUpperCase()}</CardHeader>
                            <ImagenContainer style={{ backgroundImage: fondoPkm }}>
                                <CardMainImg src={pokemon.sprites.front_default} />
                            </ImagenContainer>
                            <InfoContainer>
                                <NumberInfo>#{pokemon.id}</NumberInfo>
                                Tipos:
                                <TypesContainer>
                                    {pokemon.types.map((typeObj, idx) => {
                                        const { type } = typeObj
                                        switch (type.name) {
                                            case "fire":
                                                return (<FireType key={idx}>FUEGO</FireType>)
                                            case "water":

                                                return (<WaterType key={idx}>AGUA</WaterType>)
                                            case "grass":

                                                return (<PlantType key={idx}>PLANTA</PlantType>)
                                            case "electric":

                                                return (<ElectricType key={idx}>ELECTRICO</ElectricType>)

                                            default:
                                                return (<OtherType key={idx}>{type.name.toUpperCase()}</OtherType>)
                                        }
                                    })}
                                </TypesContainer>
                                <PreFormLabel>Forma Previa: Charmeleon</PreFormLabel>
                            </InfoContainer>
                        </CardContainer>
                    )
                })}
            </CardsContainer>
        </ContentContainer>)
    }
})
