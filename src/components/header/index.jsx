import React, { useState } from 'react';
import { HeaderContainer, HeaderLogo, HeaderSearchButton, HeaderSearchContainer, HeaderSearchInput } from '../styles/header';
import logo from '../assets/pokeLogo.png'
import { getPokemonData } from '../services/pokeapi';
const POKE_API = process.env.REACT_APP_POKE_API_URL

export const HeaderComponent = ({ showDetail }) => {
    const [name, setName] = useState('')

    const onValueChange = e => {
        setName(e.target.value)
    }

    const getPokemon = async () => {
        const pokemonData = await getPokemonData(`${POKE_API}pokemon/${name}`)
        if (pokemonData.status !== 200) return alert(`No se encontró el Pokemon ${name}`)
        showDetail(pokemonData.data)
        setName('')
    }

    return <HeaderContainer>
        <HeaderLogo src={logo} />
        <HeaderSearchContainer>
            <HeaderSearchInput value={name} onChange={onValueChange} placeholder='Busca tu pokemon...' />
            <HeaderSearchButton onClick={() => {
                if (name.length === 0) return alert('Por favor ingrese un nombre')
                getPokemon()
            }}>Buscar</HeaderSearchButton>
        </HeaderSearchContainer>
    </HeaderContainer>;
};
