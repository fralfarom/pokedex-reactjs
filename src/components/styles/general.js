import styled, { keyframes } from "styled-components";

export const FadeInAnimation = keyframes`
    from {
        opacity: 0;
    }

    to {
        opacity: 1;
    }
`

export const PrimaryButton = styled.button`
    align-self: flex-end;
    width: 250px;
    height: 30px;
    color: white;
    background-color: #3970f2;
    border: 0;
    border-radius: 5px;
`