import styled from "styled-components";
import { FadeInAnimation } from "./general";

export const DetailContainer = styled.div`
    width: 100%;
    min-height: 100vh;
    diplay:flex;
    padding: 20px 20px 0 0;
    animation: ${FadeInAnimation} 1s linear;
`
export const MainInfo = styled.div`
    width: 100%;
    display: flex;
    justify-content: space-around;
`

export const PokemonDetailImagen = styled.img`
    width: 200px;
    height: 220px;
`

export const PokemonMainInfo = styled.div`
    font-size: 25px;
    font-weight: bold;
    display: flex;
`

export const Section = styled.div`
    margin-left: 50px;
`

export const ActioBlock = styled.p`
    display: flex;
    justify-content: end;
    padding: 20px;
`

export const SectionTitle = styled.p`
    font-size: 25px;
    font-weight: bold;
`

export const PokemonSprites = styled.div`
    display: grid;
    grid-template-columns: auto auto auto auto;
    grid-gap: 25px;
`
