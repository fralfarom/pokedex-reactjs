import styled from "styled-components";
import { FadeInAnimation } from "./general";

export const ContentContainer = styled.div`
    background-color: #eef6ff;
    height: 100%;
    min-height: 100vh;
    width: 100%;
    position: relative;
    overflow-x: hidden;
    overflow-y: hidden;
`

export const CardsContainer = styled.div`
    padding: 20px 20px 30px 30px;
    animation: ${FadeInAnimation} 1s linear;
    --auto-grid-min-size: 16rem;
    display: grid;
    grid-template-columns: repeat(auto-fill, minmax(var(--auto-grid-min-size), 1fr));
    grid-gap: 1rem;
`