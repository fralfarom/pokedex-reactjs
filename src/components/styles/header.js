import styled from "styled-components";

export const HeaderContainer = styled.div`
    width: 100%;
    border-bottom: 2px solid black;
    background-color: #ff4848;
    min-height: 200px;
    position: relative;
    top: 0px;
    display: flex;
    flex-direction: column;
    justify-content: center;
    align-items: center;
`
    
export const HeaderLogo = styled.img`
    max-width: 300px;
    max-height: 80px;
`
    
export const HeaderSearchContainer = styled.div`
    margin-top: 20px;
    width: 100%;
    display: flex;
    justify-content: center;
`

export const HeaderSearchInput = styled.input`
    font-size: 20px;
    width: 30%;
    border: 1px solid #e3e3e3b8;
`

export const HeaderSearchButton = styled.button`
    background-color: #84bfff;
    border: 0px;
    margin-left: 5px;
`