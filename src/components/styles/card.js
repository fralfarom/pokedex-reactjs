import styled from "styled-components";

export const CardContainer = styled.div`
    border: 1px solid #ffda48;
    border-radius: 5px;
    height: 350px;
    width: 280px;
    display: flex;
    flex-direction: column;
    padding: 0 0 18px 0;
`

export const CardHeader = styled.div`
    border-bottom: 1px solid #dfdfdf;
    width: 280px;
    text-align: center;
    font-weight: bold;
    color: #4f4f4f;
    padding: 5px 0 5px 0;
`

export const ImagenContainer = styled.div`
    width: 100%;
    height: 150px;
    background-color: #eee;
    display: flex;
    justify-content: center;
    border-bottom: 1px solid #b7b7b7;
`

export const CardMainImg = styled.img`
    width: 60%;
    max-height: 100%;
`

export const InfoContainer = styled.div`
    background-color: white;
    width: 270px;
    padding-left: 10px;
`

export const NumberInfo = styled.p`
    color: #b0b0b0;
    font-size: 20px;
`

export const TypesContainer = styled.div`
    max-width: 100%;
    display: flex;
    flex-direction: ${props => props.flexDirection};
    overflow: auto;
`

export const FireType = styled.p`
    color: red;
    font-size: 13px;
    margin-right: 5px;
`

export const WaterType = styled.p`
    color: blue;
    font-size: 13px;
    margin-right: 5px;
`

export const PlantType = styled.p`
    color: green;
    font-size: 13px;
    margin-right: 5px;
`

export const ElectricType = styled.p`
    color: #aeae2b;
    font-size: 13px;
    margin-right: 5px;
`

export const OtherType = styled.p`
    color: black;
    font-size: 13px;
    margin-right: 5px;
`

export const PreFormLabel = styled.p`
    color: grey;
`
