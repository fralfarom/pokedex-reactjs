import { apiGET } from "./api"

const POKE_API = process.env.REACT_APP_POKE_API_URL

export const getPokemons = async(limit, offset) => {
    return apiGET(`${POKE_API}pokemon?limit=${limit}&offset=${offset}`)
}

export const getPokemonData = async(url) => {
    return apiGET(url, null)
}