import React, { useEffect, useRef, useState } from 'react';
import { ContentComponent } from '../content';
import { HeaderComponent } from '../header';
import { getPokemonData, getPokemons } from '../services/pokeapi';

export const MainComponent = () => {
    const [list, setList] = useState([])

    const contentRef = useRef()

    const getList = async () => {
        const responsePokemons = await getPokemons(25, 0)

        const pokemonArr = []
        for (const pokemon of responsePokemons.data.results) {
            pokemonArr.push(await getPokemonData(pokemon.url))
        }

        setList(pokemonArr)
    }

    useEffect(() => {
        getList()
    }, [])

    useEffect(() => {
        console.log('contentRef: ', contentRef);
    }, [contentRef])

    return <div>
        <HeaderComponent showDetail={contentRef?.current?.openDetail} />
        <ContentComponent list={list} ref={contentRef} />
    </div>;
};
