import React from 'react';
import { ElectricType, FireType, OtherType, PlantType, WaterType } from '../styles/card';
import { ActioBlock, DetailContainer, MainInfo, PokemonDetailImagen, PokemonMainInfo, PokemonSprites, Section, SectionTitle } from '../styles/detail';
import { PrimaryButton } from '../styles/general';

export const DetailComponent = ({ pokemon, closeDetail }) => {
    return <DetailContainer>
        <MainInfo>
            <PokemonDetailImagen src={pokemon.sprites.front_default} />
            <Section>
                <PokemonMainInfo>
                    <p>#{pokemon.id}-</p>
                    <p>{pokemon.name.toUpperCase()}</p>
                </PokemonMainInfo>
                <PokemonMainInfo>
                    {pokemon.types.map((typeObj, idx) => {
                        const { type } = typeObj
                        switch (type.name) {
                            case "fire":
                                return (<FireType key={idx}>FUEGO</FireType>)
                            case "water":

                                return (<WaterType key={idx}>AGUA</WaterType>)
                            case "grass":

                                return (<PlantType key={idx}>PLANTA</PlantType>)
                            case "electric":

                                return (<ElectricType key={idx}>ELECTRICO</ElectricType>)

                            default:
                                return (<OtherType key={idx}>{type.name.toUpperCase()}</OtherType>)
                        }
                    })}
                </PokemonMainInfo>
            </Section>
            <Section>
                <SectionTitle>HABILIDADES:</SectionTitle>
                <ul>
                    {pokemon.abilities.map(abilityObj => (
                        <li key={abilityObj.ability.name}>{abilityObj.ability.name}</li>
                    ))}
                </ul>
            </Section>
            <Section>
                <SectionTitle>SPRITES:</SectionTitle>
                <PokemonSprites>
                    {Object.keys(pokemon.sprites).map((sprite, idx) => <img key={idx} alt={''} src={pokemon.sprites[sprite]} />)}
                </PokemonSprites>
            </Section>
        </MainInfo>
        <Section>
            <SectionTitle> INFORMACIÓN GENERAL:</SectionTitle>
            <table style={{ width: '60%', textAlign: 'center' }}>
                <thead style={{ border: '1px solid' }}>
                    <tr>
                        <th style={{ border: '1px solid' }}>Peso</th>
                        <th style={{ border: '1px solid' }}>Altura</th>
                        <th style={{ border: '1px solid' }}>Base EXP</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td style={{ border: '1px solid' }}>{pokemon.weight}KG</td>
                        <td style={{ border: '1px solid' }}>{pokemon.height}CM</td>
                        <td style={{ border: '1px solid' }}>{pokemon.base_experience}exp</td>
                    </tr>
                </tbody>
            </table>
        </Section>
        <ActioBlock>
            <PrimaryButton onClick={e => closeDetail()} >Volver</PrimaryButton>
        </ActioBlock>
    </DetailContainer>;
};
