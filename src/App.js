import './App.css';
import {
  Routes,
  Route,
} from "react-router-dom";
import { MainComponent } from './components/main';

function App() {
  return (
    <Routes>
      <Route path="/" element={<MainComponent />} />
    </Routes>
  );
}

export default App;
